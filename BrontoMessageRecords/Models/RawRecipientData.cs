﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrontoMessageRecords.Controllers;

namespace BrontoMessageRecords.Models
{
    public class RawRecipientData
    {
        public int ID { get; set; }

        public string ActivityType { get; set; }

        public string AutomatorName { get; set; }

        public string BounceReason { get; set; }

        public string BounceType { get; set; }

        public string ContactStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? DeliveryStart { get; set; }

        public string DeliveryType { get; set; }

        public string Email { get; set; }

        public string FtafEmails { get; set; }

        public string LinkName { get; set; }

        public string LinkUrl { get; set; }

        public string ListLabel { get; set; }

        public string ListName { get; set; }

        public string MessageName { get; set; }

        public string MobileNumber { get; set; }

        public string SegmentName { get; set; }

        public string SkipReason { get; set; }

        public string SmsKeywordName { get; set; }

        public string SocialActivity { get; set; }

        public string SocialNetwork { get; set; }

        public string UnsubscribeMethod { get; set; }

        public string WebformAction { get; set; }

        public string WebformName { get; set; }

        public string WebformType { get; set; }

        public string WorkflowName { get; set; }

        public string OrderIdBronto { get; set; }

        public string ContactIdBronto { get; set; }

        public string DeliveryIdBronto { get; set; }

        public string ListIdBronto { get; set; }

        public string SegmentIdBronto { get; set; }

        public string KeywordIdBronto { get; set; }

        public string MessageIdBronto { get; set; }

        public string WorkflowIdBronto { get; set; }

        public string WebformIdBronto { get; set; }

        public bool? EngageEmailToNetSuite { get; set; }

        public string SubjectLine { get; set; }

    }

}
