﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Net;
using BrontoMessageRecords.Models;

namespace BrontoMessageRecords.Controllers
{
    public class NetsuiteController
    {
        public enum MessageType
        {
            OrderConfirmation = 1,
            ShippingConfirmation,
            EstimateEmail,
            EstimateFollowUp,
            AlbertAndPRORegistration,
            PasswordReset,
            PasswordUpdate,
            ProtectionExpiration,
            PROOrderFollowUp,
            PromoRep,
            PromoCompany,
            WelcomeEmail,
            AbandonedCart,
            LeadGen
        };
        public enum EmailSource
        {
            NetSuite = 1,
            Website,
            BrontoWorkflowSend,
            BrontoMarketingSend
        };

        public static string CreateBrontoMessageRecord(RawRecipientData bmr, MessageType messageType)
        {
            string url = "https://634494.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=1645&deploy=2&compid=634494&h=58679c9289d27648ba7a";

            var parameters = new 
            {
                email = bmr.Email,
                subject = bmr.SubjectLine,
                sendMessage = false,
                messageType,
                source = messageType == MessageType.PromoRep || 
                            messageType == MessageType.PromoCompany || 
                                messageType == MessageType.AbandonedCart ? EmailSource.BrontoMarketingSend :
                                    messageType == MessageType.PROOrderFollowUp ||    
                                        messageType == MessageType.ProtectionExpiration ||  
                                            messageType == MessageType.AlbertAndPRORegistration || 
                                                messageType == MessageType.LeadGen ||
                                                    messageType == MessageType.WelcomeEmail ? EmailSource.BrontoWorkflowSend : 0,
                brontoResponse = $"Success, Email Sent to {bmr.Email}",
                messageContent = ""
            };

            JObject brontoMessageRecord = JObject.FromObject(parameters);
            string result = null;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (var client = new MyWebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = client.UploadString(url, brontoMessageRecord.ToString());
            }

            return string.IsNullOrEmpty(result) ? "" : result;
        }
    }

    class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 900000;
            return w;
        }
    }
}