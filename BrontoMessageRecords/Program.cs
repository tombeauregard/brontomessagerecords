﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using BrontoMessageRecords.Controllers;
using BrontoMessageRecords.Models;
using Dapper;

namespace BrontoMessageRecords
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var transactionalWorkflowSends = CreateBrontoMessageRecord(false);
                var triggerAndBulk = CreateBrontoMessageRecord(true);
            }
            catch (Exception ex)
            {
                TeamsHelper.SendError("BRONTO - Error creating Workflow/Marketing BMR Record", ex.ToString());
            }
        }

        private static readonly string brontoConnectionString = @"Data Source=srv-pro-sqls-02;Initial Catalog=BRONTO;Integrated Security=True";
        private static async Task CreateBrontoMessageRecord(bool marketing)
        {

            int batch = 300;
            List<RawRecipientData> recordsToCreate = new List<RawRecipientData>();
            List<RawRecipientData> recordsCreated = new List<RawRecipientData>();

            while (batch == 300)
            {
                if (marketing)
                {
                    using (var conn = new SqlConnection(brontoConnectionString))
                    {
                        conn.Open();
                        recordsToCreate = conn.Query<RawRecipientData>("SELECT TOP 300 * FROM dbo.RawRecipientData WHERE ActivityType = 'send' AND DeliveryType in ('bulk', 'trigger', 'automator', 'split', 'ftaf') AND EngageEmailToNetSuite = 0").ToList();
                        conn.Close();
                    }
                }
                else
                {
                    using (var conn = new SqlConnection(brontoConnectionString))
                    {
                        conn.Open();
                        recordsToCreate = conn.Query<RawRecipientData>("SELECT TOP 300 * FROM dbo.RawRecipientData WHERE ActivityType = 'send' AND DeliveryType = 'transaction' AND MessageName like '%Second Send%' AND EngageEmailToNetSuite = 0").ToList();
                        conn.Close();
                    }
                }
                if (recordsToCreate.Count > 0)
                {
                    Parallel.ForEach((recordsToCreate), new ParallelOptions { MaxDegreeOfParallelism = 40 }, record =>
                    {
                        var result = record.MessageName.Contains("PRO Order Follow-Up") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.PROOrderFollowUp) :
                            record.MessageName.Contains("Protection Expiration") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.ProtectionExpiration) :
                                record.MessageName.Contains("Promo - Company") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.PromoCompany) :
                                    record.MessageName.Contains("Promo - Rep") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.PromoRep) :
                                        record.MessageName.Contains("Albert and PRO Registration") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.AlbertAndPRORegistration) :
                                            record.MessageName.Contains("PRO Welcome and Onboarding") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.WelcomeEmail) :
                                                record.MessageName.Contains("Lead Gen") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.LeadGen) :
                                                    record.MessageName.Contains("Abandoned Cart") ? NetsuiteController.CreateBrontoMessageRecord(record, NetsuiteController.MessageType.AbandonedCart) : NetsuiteController.CreateBrontoMessageRecord(record, 0);

                        if (!string.IsNullOrEmpty(result))
                        {
                            record.EngageEmailToNetSuite = true;
                            recordsCreated.Add(record);
                        }
                    });
                    using (var conn = new SqlConnection(brontoConnectionString))
                    {
                        conn.Open();
                        conn.Execute("UPDATE dbo.RawRecipientData SET EngageEmailToNetSuite = @EngageEmailToNetSuite WHERE Email=@Email AND ActivityType ='send' AND DeliveryIdBronto=@DeliveryIdBronto AND CreatedDate=@CreatedDate", recordsCreated);
                        conn.Close();
                    }
                }
                batch = recordsToCreate.Count;
                recordsToCreate.Clear();
                recordsCreated.Clear();
            }
        }
    }
}
