# readme #
  
### what is this repository for? ###
  
* This project reads email send activity from srv-pro-sqls-02.BRONTO.dbo.RawRecipientData and creates Bronto Message Records in NetSuite for sends from the Bronto UI.
  
### how do i get set up? ###
  
* summary of set up
  * Runs on SRV-PRO-SCHD-01 hourly
  * Selects all marketing and workflow sends from Bronto in batches of 1000
  * Calls the CreateBrontoMessageSuitelet in NetSuite to create the record of the send in NetSuite
* configuration
  * Nuget Packages 
      * Dapper
      * Helper
      * System.Data.SqlClient
      * Newtonsoft.Json
    
* non-nuget dependencies
* database configuration
    * SRV-PRO-SQLS-02.BRONTO
* how to run tests
* deployment instructions
    * Copy and paste the debug folder into the script on Scheduler 01
    * Deployed on Scheduler 01
    * Hourly
* connected APIs/webservices
    * Uses the bronto service account on SCHD-01 for CREDS - talk to IT
* entry points
    * the Executable is called from the scripts on SCHD-01
### who do i talk to? ###
  
* Tom Beauregard
* t.beauregard@hmwallace.com